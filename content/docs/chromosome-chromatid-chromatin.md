---
title: "Chromosome Chromatid Chromatin etc.. related terms"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{< youtube id="s9HPNwXd9fk?t=539" >}}

{{<orbit>}}
<orbit-reviewarea color="brown">

<orbit-prompt
question="what's the name of the `wrapped-around` proteins in chromosomes?"
answer="histones (9:50)"
></orbit-prompt>

<orbit-prompt
question="DNA + ?? = Chromatin"
answer="Proteins (the histone)"
></orbit-prompt>

<orbit-prompt
question="?? + Proteins = Chromatin"
answer="DNA"
></orbit-prompt>

<orbit-prompt
question="DNA + Proteins = ??"
answer="Chromatin"
></orbit-prompt>

<orbit-prompt
question="When during cell division, each chromosome tries to split into two pieces... they are initially joined at a point called ??"
answer="Centromere"
></orbit-prompt>

<orbit-prompt
question="When during cell division, each chromosome tries to split into two pieces... each single piece of the split up single piece of chromosome is called?"
answer="Chromatid"
></orbit-prompt>

</orbit-reviewarea>
{{<orbit>}}

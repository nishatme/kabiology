---
title: "Mitosis Meiosis and Sexual Reproduction"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

### Mitosis, Meiosis and Sexual Reproduction
{{< youtube id="kaSIjIzAtYA" >}}

{{<orbit>}}
<orbit-reviewarea color="brown">

<orbit-prompt
question="What does the word 'haploid' mean in biology?"
answer="half the total number... (meiosis cell division -> sperm and egg cells carry 23 instead of 46 chromosomes... ie. they are haploids.)"
></orbit-prompt>

<orbit-prompt
question="What does the word 'diploid' mean in biology?"
answer="Double the number.. (sperm and egg cells haploid... whereas somatic cells are diploid)"
></orbit-prompt>

<orbit-prompt
question="What is one great example of natural selection happening during our birth?"
answer="that around 200 million sperm cells all had to compete each other to reach first to the egg cell... only the fittest sperm cells survive!"
></orbit-prompt>

<orbit-prompt
question="sperm and egg cells forms ?? cell (biological term)"
answer="zygote"
></orbit-prompt>

</orbit-reviewarea>

{{</orbit>}}

### Phases of Mitosis

{{< youtube id="LLKX_4DHE3I" >}}

{{<orbit>}}
<orbit-reviewarea color="brown">
<orbit-prompt
question="In a cell, all the things outside of nucleus is called?"
answer="cytoplasm"
></orbit-prompt>
<orbit-prompt
question="is mitosis a cell division?"
answer="no. (it is just the nucleus-division... cell division = cytokinesis"
></orbit-prompt>
<orbit-prompt
question="What are the x4 steps in mitosis?"
answer="1. Prophase 2. Metaphase 3. Anaphase 4. Telophase"
></orbit-prompt>
</orbit-reviewarea>
{{</orbit>}}

### Phases of Meiosis
{{< youtube id="ijLc52LmFQg" >}}
{{<orbit>}}
<orbit-reviewarea color="brown">
<orbit-prompt
question=""
answer=""
></orbit-prompt>
</orbit-reviewarea>
{{</orbit>}}
